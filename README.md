# Fullstack Engineer Challenge

## Idea of the App 

The task is to implement a small webapp that will store the current temperature of two cities when a user logs in, and display a historical list of the users login temperatures. 

## Technologies to use

* PHP (Laravel 8.x)
* SQLite
* Vue.js, React (our team uses Vue.js)
* Open Weather API / https://openweathermap.org/api/one-call-api

## Acceptance criteria

* Set up a vanilla project (Laravel 8.x)
* Add the relevent info of two cities of choice to a `config` file (for the api calls, and page headings etc.)
* A user must be able to register on the platform
* A user must be able to login to the platform
* On each successful login, the current temperature for each city must be pulled from the OpenWeatherMap API and stored in the database
* The users historical list of temperatures must be displayed for each city in chronological order by default
* Each user of the platform must only see their own login temperature recordings
* Each temperature must be displayed in both Celsius (°C) and Fahrenheit (°F)
* On clicking the “Hottest First” button, the list of temperatures must be ordered Hottest to Coldest
* On clicking the “Reset Order” button, the list of temperatures must be reset to chronological order

## Not in scope

* Forgot password functionality is not in scope
* Changing the city after initial setup is not in scope

## Things to keep in mind 🚨

* Feel free to use any packages available to bootstrap your webapp (eg. if using Laravel: Laravel Breeze, Laravel Jetstream)
* Features are less important than code quality. Put more focus on code quality and less on speed, and the number of features implemented. 
* Your code will be evaluated based on: code structure, security, programming best practices, legibility (and not the number of features implemented or speed). 
* The git commit history (and git commit messages) will also be evaluated.
* Please include an overview of your solution and the reasons for your decisions (patterns, libraries etc.) in the README.
* Please include instructions on how to set up, configure and run the project locally in the README.

## How to get the API Key for OpenWeatherMap 

The API Key is provided in the Coderbyte project description. 

## Mockup

![CodeChallange](https://bitbucket.org/udifyco/fullstack-engineer-challenge/raw/master/CodeChallenge.png)

## How to submit the challenge solution? 

Once you have finished your app and pushed it to your GitHub account, please follow the instructions below:

1. Set the Repo privacy to public. If you have any concerns with this let us know.
2. Add the repo link to Coderbyte.
